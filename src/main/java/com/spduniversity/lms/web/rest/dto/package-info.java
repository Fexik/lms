/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.spduniversity.lms.web.rest.dto;
